<?php

declare(strict_types=1);
function maskify(string $cc): string
{
    if (strlen($cc) < 7) {
        return $cc;
    }
    return preg_replace('/([0-9])[0-9]{3}(-?)[0-9]{4}(-?)([0-9]{4})/', '\1###\2####\3\4', $cc);
}
echo();