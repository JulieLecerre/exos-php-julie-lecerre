<?php

class Robot {
    public function __construct() {
        if (!isset($GLOBALS['robots'])) {
            global $robots;

            $robots = [];
        }

        $this->robots = $GLOBALS['robots'];
        $this->name = null;
        $this->setName();
    }

    public function setName() :void {
        $this->reset();
    }

    public function getName() :string {
        return $this->name;
    }

    public function reset(): void {
        $characterNumber = '0123456789';
        $characterLetter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = $characterLetter[rand(0, 25)].$characterLetter[rand(0, 25)];
        $randomString .= $characterNumber[rand(0,9)].$characterNumber[rand(0,9)].$characterNumber[rand(0,9)];

        $this->name = $randomString;

        while(in_array($this->name, $this->robots)) {
            $this->reset();
        }

        array_push($GLOBALS['robots'], $randomString);

    }
}

